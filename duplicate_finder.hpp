#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/ml.hpp>

#include <memory>
#include <iostream>

///---------- Duplicate retrieval
class DF {
private:
    std::string inp_dir;
    std::string out_dir;

    int num_files;
    std::vector<cv::String> files;

    cv::Mat hash_table;
    cv::Mat hash_labels;
    std::shared_ptr<Hash> hash;
    cv::Ptr<cv::ml::KNearest> knn;
    float cutoff;

    int K;
    cv::Size thumb_size;

    DF( const std::string& input,
        const std::string& output) :
        inp_dir(input), out_dir(output)
    {
        thumb_size = cv::Size(64, 64);
        try {
            cv::glob( cv::String(inp_dir + "/*.jpg"), files, true );
        } catch(...) {
            std::cout << "Failed to read input files...\n";
        }
        num_files = files.size();
    }
public:
    static std::shared_ptr<DF> createInstance(const std::string& input,
                                              const std::string& output) {
        static std::shared_ptr<DF> instance( new DF( input, output ) );
        return instance;
    }

    void createHashTable() {
        //
        std::cout << "Creating hash table...\n";
        for(int idx = 0; idx < num_files; idx++) {
            cv::Mat h = hash->compute( files[idx] );
            h.copyTo( hash_table.row(idx) );
            hash_labels.row(idx) = idx;

        }
        std::cout << "Hash table created in : " << (hash->T()/float(hash->C())) << " ms\n";
    }
    void calcSimilarThreshold( const float cut_ratio=0.035 ) {
        cutoff = 0.0;
        cv::RNG rng( cv::getTickCount() );
        int num_tests = std::min( 100, num_files );
        for(int i = 0; i < num_tests; i++)
        {
            int idx1 = rng.uniform( 0, num_files );
            int idx2 = rng.uniform( 0, num_files );
            cutoff += cv::norm( hash_table.row(idx1), hash_table.row(idx2), cv::NORM_L2SQR );
        }
        cutoff /= float( num_tests );
        cutoff *= cut_ratio;
        std::cout << "Similarity threshold distance " << cutoff << "\n";
    }
    ///---------- train KNN
    void train() {
        std::cout << "Training KNN...\n";
        knn->setDefaultK(K);            // number of retrieved similar indices
        knn->setIsClassifier(false);    // set false for retrieval
        //knn->setAlgorithmType(cv::ml::KNearest::KDTREE);
        knn->train( hash_table, cv::ml::ROW_SAMPLE, hash_labels );
        std::cout << "KNN trained\n";
    }

    ///---------- find similar images
    void search() {

        int duplicates = 0;
        cv::Mat mdists, mlabels, mneigh;
        float t_avg = 0.0;
        float t = 0.0;
        for( int f = 0; f < num_files; f++ )
        {
            t = (float)cv::getTickCount();

            knn->findNearest( hash_table.row(f), K, mlabels, mneigh, mdists );
            t_avg += ((float)cv::getTickCount() - t)/cv::getTickFrequency()*1000.0;
            //--- create output duplicate images
            std::vector<cv::Mat> out_imgs;
            for(int k = 0; k < K; k++)
            {
                int midx   = mneigh.at<float>(k);
                float dist = mdists.at<float>(k);

                if((midx > f) && (dist < cutoff))
                {
                    cv::Mat dup_img;
                    dup_img = cv::imread( files[midx] );
                    if( !dup_img.data )
                    {
                        dup_img = cv::Mat::zeros( thumb_size, CV_8UC3 );
                    }else{
                        cv::resize( dup_img, dup_img, thumb_size);
                    }
                    out_imgs.push_back( dup_img );
                }
            }
            if( out_imgs.size() > 0 )
            {
                // load original image
                cv::Mat src_img = cv::imread( files[f] );
                cv::resize( src_img, src_img, thumb_size );

                // create separator
                cv::Mat div = cv::Mat::zeros( thumb_size.height, 2, CV_8UC3 );
                out_imgs.push_back( div );
                out_imgs.push_back( src_img );

                // avoid 'panorams'
                if( out_imgs.size() > 10 ) out_imgs.resize(10);

                // concatenate images to single one
                cv::Mat out_img;
                cv::hconcat( out_imgs, out_img );

                cv::imwrite( out_dir + '/' + std::to_string(f) + ".jpg", out_img );
                duplicates += out_imgs.size();
            }
            // output
            if( f%100 == 0 )
            {
                std::cout << f << "/" << num_files << ", ms/match : " << t_avg/100.0 << " ms, dupl : " << duplicates << "\n";
                t_avg = 0.0;
            }
        }
        std::cout << "duplicates : " << duplicates << "\n";
    }
    void run( HashID id,
              const int n_rects=16,
              cv::Size block_size=cv::Size(4, 4),
              float cut_ratio=0.035,
              cv::Size img_size=cv::Size(128, 128),
              const int num_retrieved=25) {

        // create hash function instance
        hash = Hash::create( id );
        hash->init( n_rects, img_size, block_size );

        hash_table  = cv::Mat::zeros( num_files, hash->featureSize(), CV_32FC1 );
        hash_labels = cv::Mat::zeros( num_files, 1, CV_32SC1);

        knn = cv::ml::KNearest::create();
        K = std::max( 10, std::min( num_retrieved, num_files ) );

        createHashTable();
        calcSimilarThreshold( cut_ratio );
        train();
        search();
    }

    ~DF() {}
};
