#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <memory>
#include <iostream>

enum HashID{ H1_ID=0, H2_ID };
class Hash {
protected:
    int dim;
    float time;
    int count;
public:
    float C() const {return count;}
    float T() const {return time;}
    virtual int featureSize()=0;
    virtual void init( const int n_rects = 16,
                       cv::Size input_s=cv::Size(128,128),
                       cv::Size block_s=cv::Size(4,4) )=0;
    virtual cv::Mat compute( const std::string& src )=0;
    virtual ~Hash() {}
    static std::shared_ptr<Hash> create( HashID id );
};

class H1 : public Hash {
private:
    std::vector<int> quant;
    std::vector<cv::Rect> rects;
    cv::Size input_size;
    cv::Size block_size;
    int block_area;
    int num_rects;

public:
    int featureSize() {
        return dim;
    }

    void init( const int n_rects = 16,
               cv::Size input_s=cv::Size(128,128),
               cv::Size block_s=cv::Size(4,4) ) {
        input_size = input_s;
        block_size = block_s;
        block_area = block_size.area();
        num_rects = n_rects;
        cv::Point2i p1, p2;
        cv::RNG rng( cv::getTickCount() );
        for(int i = 0; i < num_rects; i++)
        {
            float cx = rng.uniform(0.15, 0.85) * input_size.width;
            float cy = rng.uniform(0.15, 0.85) * input_size.height;
            p1.x = rng.uniform( -0.15, -0.1 ) * input_size.width + cx;
            p1.y = rng.uniform( -0.15, -0.1 ) * input_size.height + cy;
            p1.x = std::max( 0, std::min( p1.x, input_size.width ));
            p1.y = std::max( 0, std::min( p1.y, input_size.height ));

            p2.x = rng.uniform( 0.1, 0.15 ) * input_size.width + cx;
            p2.y = rng.uniform( 0.1, 0.15 ) * input_size.height + cy;
            p2.x = std::max( 0, std::min( p2.x, input_size.width ));
            p2.y = std::max( 0, std::min( p2.y, input_size.height ));
            rects.push_back( cv::Rect(p1, p2) );
            quant.push_back( rng.uniform( 8, 64 ) );
        }
        // calculated feature size
        dim = num_rects * block_size.area() * 3;
    }

    cv::Mat compute( const std::string& src ) {
        float t = (float)cv::getTickCount();
        cv::Mat h;
        cv::Mat inp = cv::imread( src );
        if( !inp.data ) {
            h = cv::Mat::zeros(1, dim, CV_8UC1);
            return h;
        }
        cv::resize(inp, inp, input_size);
        //--- hash computation
        int block_chan = inp.channels();

        // reduce image quality to suppress noise and small artifacts
        cv::Mat tmp, block;
        cv::resize( inp, tmp, cv::Size(32, 32), 0.0, 0.0, cv::INTER_NEAREST );
        cv::resize( tmp, tmp, input_size, 0.0, 0.0, cv::INTER_NEAREST);

        // normalize to 0..1
        tmp.convertTo(tmp, CV_32FC3);
        tmp /= 255.0;

        // output vector
        h = cv::Mat::zeros(1, 0, CV_8UC1);
        for(int i = 0; i < num_rects; i++)
        {
            tmp( rects[i] ).copyTo( block );

            // quantize values in block to 0..quant[i] levels
            cv::normalize( block, block, 0.0, 1.0, cv::NORM_MINMAX );
            block *= quant[i];
            block.convertTo(block, CV_8UC3);

            // append to feature vector
            cv::resize( block, block, block_size );
            h.push_back( block.reshape(1, block_area*block_chan) );
        }
        h.reshape( 1, dim ).copyTo( h );
        cv::transpose( h, h );

        //--- eo hash computation
        time += (float(cv::getTickCount()) - t)/float(cv::getTickFrequency())*1000.0f;
        count++;
        return h;
    }
    virtual ~H1() { std::cout << "H1 destroyed\n"; }
};

std::shared_ptr<Hash> Hash::create(HashID id) {
    std::shared_ptr<Hash> p;
    if( id == H1_ID ) {
        p = std::make_shared<H1>();
    }else{
        p = nullptr;
    }
    return p;
}
