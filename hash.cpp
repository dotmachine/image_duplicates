#include <iostream>
#include <memory>

#include "image_hash.hpp"
#include "duplicate_finder.hpp"


int main() {
    std::shared_ptr<DF> df = DF::createInstance("input", "output");
    df->run( H1_ID, 10, cv::Size(4, 4), 0.035 );
    //df->run( H1_ID, 10, cv::Size(4, 4), 0.035 );
    return 0;
}
